<?php

class ProcessingDataCSV implements iProcessingData
{
    private $filename;
    function __construct(string $filename)
    {
        $this->filename = "data/".$filename;
    }

    public function read()
    {
        $result = array();

        foreach ($this->readLine() as $line) {

            $data = explode("	",  $line);
            if(trim($data[2]) == "Winning Bid (Revenue)"){
                if(empty($data[18])) continue;  // if empty order id then next iteration

                $datetime   = $data[11];
                $sum        = empty($data[15]) ? "0.0" : ($data[15][0] == ".") ? "0".$data[15] : $data[15] ;

                $result[] = new Order(array(1, $data[18], "approved", $sum, "USD", $datetime));

            }
        }
        return $result;
    }

    private function readLine() {
        $file = fopen($this->filename, 'r');
        while (($line = fgets($file)) !== false) {
            yield $line;
        }
        fclose($file);
    }
}
