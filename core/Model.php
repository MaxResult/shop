<?php

namespace base\core;

use PDO;
use Exception;

require_once 'DbConfig.php';

class Model extends DbConfig{

    protected $dbc; //db connect

    function __construct(){
        $this->DBConnect();
    }

    protected function DBConnect(){

        if(!empty($this->dbc)){
            return $this->dbc;
        }

        $dsn = "mysql:host=".$this->host.";dbname=".$this->dbName;
        try{
            $db = new PDO ($dsn, $this->user, $this->pass);
            $db->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
            $db->setAttribute(PDO::MYSQL_ATTR_USE_BUFFERED_QUERY, TRUE);
            $db->setAttribute(PDO::ATTR_DEFAULT_FETCH_MODE, PDO::FETCH_ASSOC);

            $this->dbc = $db;

            return $db;
        } catch(Exception $e){
            die ($e->getMessage());
        }
    }
}