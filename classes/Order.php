<?php

class Order{
    public $advcampaign_id;
    public $order_id;
    public $status;
    public $cart;
    public $currency;
    public $action_date;

    function __construct($data)
    {
        $this->advcampaign_id = $data[0];
        $this->order_id = $data[1];
        $this->status = $data[2];
        $this->cart = $data[3];
        $this->currency = $data[4];
        $this->action_date = $data[5];
    }
}

