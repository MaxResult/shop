<?php
declare(strict_types=1);

interface iProcessingData
{

    public function read();
    
}