<?php

declare(strict_types=1);

//include core
include_once 'core/Model.php';

function __autoload($className){
    $dirs = array(
        'model/',
        'classes/'
    );

    $parts = explode('\\', $className);
    $end = end($parts);

    foreach($dirs as $dir){
        if(file_exists($dir . $className . ".php")){
            require_once($dir . $end. ".php");
        }
    }
}

//files that will be read and processed
$readFiles = array(
    array('file' => 'csv (1).csv', 'service_id' => 2),
    array('file' => 'xml (1).xml', 'service_id' => 1)
);

$OrderModel = new OrderModel();
foreach ($readFiles as $val){
    $parts = explode('.', $val['file']);
    $ext = end($parts);   // get extension of the file

    $class = "ProcessingData".strtoupper($ext);
    try{
        $processData = new $class($val['file']);
        $data = $processData->read();
        foreach ($data as $value){
            $value->cart = ($value->cart == "-") ? "0.0" :  $value->cart ;

            $result = $OrderModel->select($value, $val['service_id']);
            if(empty($result)){
                echo "insert company {$value->advcampaign_id} with order {$value->order_id} and service_id {$val['service_id']}\n";
                $OrderModel->insert($value, $val['service_id']);
            }elseif($result['status'] != $value->status){
                echo "update company {$value->advcampaign_id} with order {$value->order_id} and service_id {$val['service_id']}\n";
                $OrderModel->update($value, $val['service_id']);
            }else{
                echo "try to update company {$value->advcampaign_id} with order {$value->order_id} and service_id {$val['service_id']}\n";
            }
        }
    }catch (Error $e){
        echo "Sorry, but class $class doesn't exist\n";
        echo $e;
    }

}
