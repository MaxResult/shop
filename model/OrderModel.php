<?php

use base\core\Model;

class OrderModel extends Model{

    public function insert($data, int $service_id){
        $sql ="INSERT INTO orders(
                  campaign_id, 
                  order_id, 
                  status, 
                  cart,
                  currency, 
                  action_date,
                  service_id
                  ) 
                VALUES(?,?,?,?,?,?,?)";

        $dbo = $this->DBConnect()->prepare($sql);
        $dbo->execute(array($data->advcampaign_id,
                            $data->order_id,
                            $data->status,
                            $data->cart,
                            $data->currency,
                            $data->action_date,
                            $service_id
        ));
    }

    public function update($data, int $service_id){
        $sql ="UPDATE orders SET
                  status = ?, 
                  cart = ?,
                  currency = ?, 
                  action_date = ?
                WHERE campaign_id = ? AND order_id = ? AND 	service_id = ?
                ";

        $dbo = $this->DBConnect()->prepare($sql);
        $dbo->execute(array(
            $data->status,
            $data->cart,
            $data->currency,
            $data->action_date,
            $data->advcampaign_id,
            $data->order_id,
            $service_id)
        );
    }

    public function select( $object , $service_id){  //$campaignId, $orderId, $cart
        $sql = "SELECT * FROM orders WHERE  campaign_id = :campaign_id AND 
                                            order_id = :order_id AND 
                                            service_id = :service_id";
        $dbo = $this->DBConnect()->prepare($sql);
        $dbo->bindParam(':campaign_id', $object->advcampaign_id);
        $dbo->bindParam(':order_id',    $object->order_id);
        $dbo->bindParam(':service_id',  $service_id);
        $dbo->execute();
        return $dbo->fetch(PDO::FETCH_ASSOC);
    }

}