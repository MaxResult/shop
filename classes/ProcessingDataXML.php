<?php

class ProcessingDataXML implements iProcessingData{
    private $filename;

    function __construct(string $filename)
    {
        $this->filename = "data/".$filename;
    }

    public function read()
    {
        return simplexml_load_file($this->filename);
    }
    
}